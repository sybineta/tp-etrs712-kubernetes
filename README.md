# TP Kubernetes 

### Projet fait par Bineta SY et Lidvine Rita GABA MAHIRE

## Introduction

Notre projet consiste à faire un recencement du nombre d'étudiants (en %) sénégalais et rwandais présents au sein d'un campus universitaire.
Pour se faire nous avons développer une application qui permet d'effectuer les votes.
La récupérations des résultats se fait grace à une base de données en parallèle.


## NB
-Il est à noter qu'il est primordiale d'utiliser une machine comportant une interface graphique afin d'effectuer les tests sur le navigateur de l'environnement et pas forcément de la machine hôte.

-Lors de nos tests nous avons remarqué que la page web ne s'affichait pas sur la machine hote mais fonctionnait si on utilisait un navigateur de la machine virtuelle installée sur le serveur de l'université

-Cette erreur peut être dûe aux différentes règles de filtrage du vpn.

## Gestion des votes

Une personne ne peut voter qu'une seule fois.
Pour tester la bonne incrémentation du nombre de votes vous pouvez utiliser un navigateur en mode privée.

## Récupération du dossier

```git clone https://...```

 
## Apply du dossier

``` microk8s.kubectl apply -f k8s ```

## Accès aux votes
Pour accéder au vote en ligne, aller sur le navigateur et taper l'url suivant

```http://192.168.141.244:31000```


## Accès aux résultats

Pour accéder aux résultats en ligne, aller sur le navigateur et taper l'url suivant

```http://192.168.141.244:31001```



